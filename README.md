Для запуска необходимо склонировать 3 проекта

```
 - git@gitlab.com:gumart/docker_3.git
 - git@gitlab.com:gumart/docker_3_1.git
 - git@gitlab.com:gumart/docker_3_2.git
```

Необходимо предварительно создать сеть в котором будет крутиться контейнер с траефиком, и другими контейнерами в которые траефик будет проксировать трафик:

```
 - docker network create traefik-net
```

Находясь в директории с проектом docker_3, запустить контейнер с траефиком используя официальный образ:

```
 - docker run --network=traefik-net -d -p 8080:8080 -p 80:80 -v $PWD/traefik.yml:/etc/traefik/traefik.yml -v /var/run/docker.sock:/var/run/docker.sock:ro traefik:2.5.5
```

Запустить сети контейнеров docker_3_1 и docker_3_2:
- Скопировать .env.example в .env

- перейдя в директорию docker_3_1(docker_3_2) выполнить команду:

```
 docker-compose up -d
```

  - Выполнить миграции

```
docker-compose exec --user www-data app1 php artisan migrate
docker-compose exec --user www-data app2 php artisan migrate
```

Два приложения будут доступны по хостам
 - appdev1.localhost
 - appdev2.localhost

Для проверки можно использовать роут: /app

Для проверки работоспособности базы данных, можно воспользоваться роутами:
- `/set-test-string/{test_string}`
- `/get-all-test-strings`